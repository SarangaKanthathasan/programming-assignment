package assignment;
public class Grading {

	public void grade(String[][] stu_details,int stu_number,int sub_number) {
		System.out.println();
		for (int i=0; i<stu_number; i++) {
			System.out.println("Student name is : "+stu_details[i][0]);
		
			for (int j=1; j<=sub_number; j++) {
		
				String marks = stu_details[i][j];
				double d=Double.parseDouble(marks);
				if (d>=75) {
					System.out.println("Grade for subject "+j+" is A");
				}else if (d>=65) {
					System.out.println("Grade for subject "+j+" is B");
				}else if (d>=55) {
					System.out.println("Grade for subject "+j+" is C");
				}else if (d>=35) {
					System.out.println("Grade for subject "+j+" is D");
				}else {
					System.out.println("Grade for subject "+j+" is F");
				}
				
			}
			System.out.println("Total for all subject is " + stu_details[i][sub_number+1]);
			System.out.println();
		}
	}

}



