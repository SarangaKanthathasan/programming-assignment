package assignment;
import java.util.Scanner;

public class Highmarks {

	public  void highest(int stu_number,String[][] stu_details) {

		Scanner sc1 = new Scanner(System.in);
		System.out.print("choice subject: ");
		int choice4= sc1.nextInt();
		
		String max = stu_details[0][choice4];
		double highmarks=Double.parseDouble(max);

		for (int i=1; i<stu_number;i++) {
			String mark = stu_details[i][choice4];
			double sub_marks=Double.parseDouble(mark);
			if (sub_marks>highmarks) {
				highmarks=sub_marks;
			}else {
				continue;
			}
		}
		System.out.print("Highest marks in subject "+ choice4 +" is "+ highmarks);		
	}

}



